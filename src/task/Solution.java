package task;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static List<String> solution(int[][] arr) {
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr[0].length - 1; j++) {
                if (arr[i][j] == arr[i + 1][j] && arr[i][j] == arr[i][j + 1]
                        && arr[i][j] == arr[i + 1][j + 1]) {
                    stringList.add("( R: " + i + " C: " + j + " )");
                }
            }
        }
        return stringList;
    }
}
