package util;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.*;
import java.util.function.Function;

public class JTableUtils {

    public static final int DEFAULT_GAP = 6;
    public static final int DEFAULT_PLUSMINUS_BUTTONS_SIZE = 22;
    public static final int DEFAULT_COLUMN_WIDTH = 40;
    public static final int DEFAULT_ROW_HEADER_WIDTH = 40;
    private static final Color TRANSPARENT = new Color(0, 0, 0, 0);
    private static final char DELETE_KEY_CHAR_CODE = 127;
    private static final Border DEFAULT_CELL_BORDER = BorderFactory.createEmptyBorder(0, 3, 0, 3);
    private static final Border DEFAULT_RENDERER_CELL_BORDER = DEFAULT_CELL_BORDER;
    private static final Border DEFAULT_EDITOR_CELL_BORDER = BorderFactory.createEmptyBorder(0, 3, 0, 2);

    private static final Map<JTable, Integer> tableColumnWidths = new HashMap<>();

    private static final NumberFormat defaultNumberFormat = NumberFormat.getInstance(Locale.ROOT);

    public static void initJTableForArray(
            JTable table, int defaultColWidth,
            boolean showRowsIndexes, boolean showColsIndexes,
            boolean changeRowsCountButtons, boolean changeColsCountButtons,
            int changeButtonsSize, int changeButtonsMargin
    ) {
        table.setCellSelectionEnabled(true);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.getTableHeader().setReorderingAllowed(false);
        table.getTableHeader().setResizingAllowed(false);
        if (!showColsIndexes && table.getTableHeader() != null) {
            table.getTableHeader().setPreferredSize(new Dimension(0, 0));
        }
        table.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        table.setShowGrid(true);
        table.setIntercellSpacing(new Dimension(1, 1));
        table.setFillsViewportHeight(false);
        table.setDragEnabled(false);
        //table.setCursor(Cursor.getDefaultCursor());
        table.putClientProperty("terminateEditOnFocusLost", true);
        DefaultTableModel tableModel = new DefaultTableModel(new String[]{"[0]"}, 1) {
            @Override
            public String getColumnName(int index) {
                return String.format("[%d]", index);
            }
        };
        table.setModel(tableModel);
        tableColumnWidths.put(table, defaultColWidth);
        recalcJTableSize(table);

        if (table.getParent() instanceof JViewport && table.getParent().getParent() instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane) table.getParent().getParent();
            if (changeRowsCountButtons || changeColsCountButtons) {
                List<Component> linkedComponents = new ArrayList<>();

                scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

                BorderLayout borderLayout = new BorderLayout(changeButtonsMargin, changeButtonsMargin);
                FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT, 0, 0);

                JPanel panel = new JPanel(borderLayout);
                panel.setBackground(TRANSPARENT);

                if (changeColsCountButtons) {
                    JPanel topPanel = new JPanel(flowLayout);
                    topPanel.setBackground(TRANSPARENT);
                    if (changeRowsCountButtons) {
                        topPanel.add(setFixedSize(new Box.Filler(null, null, null), changeButtonsSize + changeButtonsMargin, changeButtonsSize));
                    }
                    JButton minusButton = createPlusMinusButton("\u2013", changeButtonsSize);
                    minusButton.setName(table.getName() + "-minusColumnButton");
                    minusButton.addActionListener((ActionEvent evt) -> {
                        tableModel.setColumnCount(tableModel.getColumnCount() > 0 ? tableModel.getColumnCount() - 1 : 0);
                        recalcJTableSize(table);
                    });
                    topPanel.add(minusButton);
                    linkedComponents.add(minusButton);
                    topPanel.add(setFixedSize(new Box.Filler(null, null, null), changeButtonsMargin, changeButtonsSize));
                    JButton plusButton = createPlusMinusButton("+", changeButtonsSize);
                    plusButton.setName(table.getName() + "-plusColumnButton");
                    plusButton.addActionListener((ActionEvent evt) -> {
                        tableModel.addColumn(String.format("[%d]", tableModel.getColumnCount()));
                        recalcJTableSize(table);
                    });
                    topPanel.add(plusButton);
                    linkedComponents.add(plusButton);

                    panel.add(topPanel, BorderLayout.NORTH);
                }
                if (changeRowsCountButtons) {
                    JPanel leftPanel = setFixedSize(new JPanel(flowLayout), changeButtonsSize, changeButtonsSize);
                    leftPanel.setBackground(TRANSPARENT);
                    JButton minusButton = createPlusMinusButton("\u2013", changeButtonsSize);
                    minusButton.setName(table.getName() + "-minusRowButton");
                    minusButton.addActionListener((ActionEvent evt) -> {
                        if (tableModel.getRowCount() > 0) {
                            tableModel.removeRow(tableModel.getRowCount() - 1);
                            recalcJTableSize(table);
                        }
                    });
                    leftPanel.add(minusButton);
                    linkedComponents.add(minusButton);
                    leftPanel.add(setFixedSize(new Box.Filler(null, null, null), changeButtonsSize, changeButtonsMargin));
                    JButton plusButton = createPlusMinusButton("+", changeButtonsSize);
                    plusButton.setName(table.getName() + "-plusRowButton");
                    plusButton.addActionListener((ActionEvent evt) -> {
                        tableModel.setRowCount(tableModel.getRowCount() + 1);
                        recalcJTableSize(table);
                    });
                    leftPanel.add(plusButton);
                    linkedComponents.add(plusButton);

                    panel.add(leftPanel, BorderLayout.WEST);
                }
                table.setPreferredScrollableViewportSize(null);
                JScrollPane newScrollPane = new JScrollPane(table);
                newScrollPane.setBackground(scrollPane.getBackground());
                newScrollPane.setBorder(scrollPane.getBorder());
                scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                panel.add(newScrollPane, BorderLayout.CENTER);

                scrollPane.getViewport().removeAll();
                scrollPane.add(panel);
                scrollPane.getViewport().add(panel);

                // привязываем обработчик событий, который активирует и дективирует зависимые
                // компоненты (кнопки) в зависимости от состояния table
                table.addPropertyChangeListener((PropertyChangeEvent evt) -> {
                    if ("enabled".equals(evt.getPropertyName())) {
                        boolean enabled = (boolean) evt.getNewValue();
                        linkedComponents.forEach((comp) -> comp.setEnabled(enabled));
                        if (!enabled) {
                            table.clearSelection();
                        }
                    }
                });
                linkedComponents.forEach((comp) -> comp.setEnabled(table.isEnabled()));

                // иначе определенные проблемы с прозрачностью panel возникают
                scrollPane.setVisible(false);
                scrollPane.setVisible(true);

                scrollPane = newScrollPane;
            }
        }
    }

    private static JButton createPlusMinusButton(String text, int size) {
        JButton button = new JButton(text);
        setFixedSize(button, size, size).setMargin(new Insets(0, 0, 0, 0));
        button.setFocusable(false);
        button.setFocusPainted(false);
        return button;
    }

    private static <T extends JComponent> T setFixedSize(T comp, int width, int height) {
        Dimension d = new Dimension(width, height);
        comp.setMaximumSize(d);
        comp.setMinimumSize(d);
        comp.setPreferredSize(d);
        comp.setSize(d);
        return comp;
    }

    private static int getColumnWidth(JTable table) {
        Integer columnWidth = tableColumnWidths.get(table);
        if (columnWidth == null) {
            if (table.getColumnCount() > 0) {
                columnWidth = table.getWidth() / table.getColumnCount();
            } else {
                columnWidth = DEFAULT_COLUMN_WIDTH;
            }
        }
        return columnWidth;
    }

    private static void recalcJTableSize(JTable table) {
        int width = getColumnWidth(table) * table.getColumnCount();
        int height = 0, rowCount = table.getRowCount();
        for (int r = 0; r < rowCount; r++)
            height += table.getRowHeight(height);
        setFixedSize(table, width, height);

        if (table.getParent() instanceof JViewport && table.getParent().getParent() instanceof JScrollPane) {
            JScrollPane scrollPane = (JScrollPane) table.getParent().getParent();
            if (scrollPane.getRowHeader() != null) {
                Component rowHeaderView = scrollPane.getRowHeader().getView();
                if (rowHeaderView instanceof JList) {
                    ((JList) rowHeaderView).setFixedCellHeight(table.getRowHeight());
                }
                scrollPane.getRowHeader().repaint();
            }
        }
    }

    private static void writeArrayToJTable(JTable table, Object array, String itemFormat) {
        if (!array.getClass().isArray()) {
            return;
        }
        if (!(table.getModel() instanceof DefaultTableModel)) {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) table.getModel();

        tableColumnWidths.put(table, getColumnWidth(table));

        if (itemFormat == null || itemFormat.length() == 0) {
            itemFormat = "%s";
        }
        int rank = 1;
        int len1 = Array.getLength(array), len2 = -1;
        if (len1 > 0) {
            for (int i = 0; i < len1; i++) {
                Object item = Array.get(array, i);
                if (item != null && item.getClass().isArray()) {
                    rank = 2;
                    len2 = Math.max(Array.getLength(item), len2);
                }
            }
        }
        tableModel.setRowCount(rank == 1 ? 1 : len1);
        tableModel.setColumnCount(rank == 1 ? len1 : len2);
        for (int i = 0; i < len1; i++) {
            if (rank == 1) {
                tableModel.setValueAt(String.format(itemFormat, Array.get(array, i)), 0, i);
            } else {
                Object line = Array.get(array, i);
                if (line != null) {
                    if (line.getClass().isArray()) {
                        int lineLen = Array.getLength(line);
                        for (int j = 0; j < lineLen; j++) {
                            tableModel.setValueAt(String.format(itemFormat, Array.get(line, j)), i, j);
                        }
                    } else {
                        tableModel.setValueAt(String.format(itemFormat, Array.get(array, i)), 0, i);
                    }
                }
            }
        }
        recalcJTableSize(table);
    }

    public static void writeArrayToJTable(JTable table, int[][] array) {
        writeArrayToJTable(table, array, "%d");
    }

    public static <T> T[][] readMatrixFromJTable(
            JTable table, Class<T> clazz, Function<String, ? extends T> converter,
            boolean errorIfEmptyCell, T emptyCellValue
    ) {
        TableModel tableModel = table.getModel();
        int rowCount = tableModel.getRowCount(), colCount = tableModel.getColumnCount();
        T[][] matrix = (T[][]) Array.newInstance(clazz, rowCount, colCount);
        for (int r = 0; r < rowCount; r++) {
            for (int c = 0; c < colCount; c++) {
                T value = null;
                Object obj = tableModel.getValueAt(r, c);
            }
            return matrix;
        }
        return matrix;
    }

    /**
     * Чтение данных из JTable в двухмерный массив
     */
    public static <T> T[][] readMatrixFromJTable(
            JTable table, Class<T> clazz, Function<String, ? extends T> converter, T emptyCellValue
    ) {
        return readMatrixFromJTable(table, clazz, converter, false, emptyCellValue);
    }

    /**
     * Чтение данных из JTable в двухмерный массив
     */
    public static <T> T[][] readMatrixFromJTable(
            JTable table, Class<T> clazz, Function<String, ? extends T> converter
    ) {
        return readMatrixFromJTable(table, clazz, converter, true, null);
    }

    public static int[][] readIntMatrixFromJTable(JTable table) throws ParseException {
        Integer[][] matrix = readMatrixFromJTable(table, Integer.class, Integer::parseInt, false, 0);
        return (int[][]) Arrays.stream(matrix).map(ArrayUtils::toPrimitive).toArray((n) -> new int[n][]);
    }
}
