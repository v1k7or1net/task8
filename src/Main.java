import task.Solution;
import util.ArrayUtils;
import util.SwingUtils;

import java.util.Locale;


public class Main {

    public static class CmdParams {
        public String inputFile;
        public String outputFile;
    }

    public static CmdParams parseArgs(String[] args) {
        CmdParams params = new CmdParams();
        if (args.length > 0) {
            for (int i = 0; i < args.length - 1; i++) {
                if (args[i].equals("--i")) {
                    params.inputFile = args[i + 1];
                }
                if (args[i].equals("--o")) {
                    params.outputFile = args[i + 1];
                }
            }
        }
        return params;
    }

    public static void winMain() throws Exception {
        //SwingUtils.setLookAndFeelByName("Windows");
        Locale.setDefault(Locale.ROOT);
        //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        SwingUtils.setDefaultFont("Microsoft Sans Serif", 18);

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrameMain().setVisible(true);
            }
        });
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 0) {
            CmdParams params = parseArgs(args);
            System.out.println(params.inputFile);
            System.out.println(params.outputFile);
            int[][] ints = ArrayUtils.readIntArray2FromFile(params.inputFile);
            if (ints != null) {
                System.out.println(Solution.solution(ints));
                ArrayUtils.saveArrayToFile(ints, params.outputFile);
            }
        } else winMain();
    }
}